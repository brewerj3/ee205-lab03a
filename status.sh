#!/bin/bash


WHO_AM_I=$(id -u -n)

echo I am

echo  $WHO_AM_I

CURRENT_DIRECTORY=$(pwd)

echo The current working directory is

echo $CURRENT_DIRECTORY

CURRENT_SYSTEM=$(uname -n)

echo The system I am on is 

echo $CURRENT_SYSTEM

CURRENT_LINUX_VERSION=$(cat /proc/version)

echo The Linux version is

echo $CURRENT_LINUX_VERSION

CURRENT_DISTRIBUTION=$(cat /etc/system-release)

echo The Linux distribution is

echo $CURRENT_DISTRIBUTION

CURRENT_UPTIME=$(uptime)

echo The System has been up \for

echo $CURRENT_UPTIME

DISK_SPACE_IN_KB=$(du -k -s ~)

echo The amount of space I\'m using \in KB is

echo $DISK_SPACE_IN_KB

