#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

read -p "OK cat , I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:..." A_GUESS

until [ $A_GUESS -eq $THE_NUMBER_IM_THINKING_OF ]
do
	if [ $A_GUESS -gt $THE_MAX_VALUE ]
	then
		read -p "You must enter a value that's <= $THE_MAX_VALUE" A_GUESS
	elif [ $A_GUESS -lt 1 ]
	then
		read -p "You must enter a value that's >= 1" A_GUESS
	elif [ $A_GUESS -gt $THE_NUMBER_IM_THINKING_OF ]
	then
		read -p "No cat... The number I'm thinking of is smaller than $A_GUESS"  A_GUESS
	elif [ $A_GUESS -lt $THE_NUMBER_IM_THINKING_OF ]
	then
		read -p "No cat... The number I'm thinking of is larger than $A_GUESS"  A_GUESS
	fi
done
echo You got me
echo "   /\**/\   "
echo "  ( o_o  )_)"
echo "  ,(u  u  ,),"
echo " {}{}{}{}{}{}"

